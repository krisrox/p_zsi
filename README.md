# Zastosowanie Sztucznej Inteligencji

## Table of contents
* [General info](#general-info)
* [Authors](#authors)

## General info

Final project for Application of Artificial Intelligence. The goal of the project is recognize polish license plate from an image using pure artificial neural network without any deep learning library.


## Authors

**Michał Staruch**
**Bartłomiej Mazurek**

========================


