import numpy as np
import cv2
import matplotlib.pyplot as plt
from processing.network import NeuralNetwork

CHARSET = ("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G",
           "H", "I", "J", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "V", "W", "X", "Y", "Z")

CHAR_HEIGHT = 30
CHAR_WIDTH = 15
SHOW_STEPS = False

nn = NeuralNetwork()


def perform_processing(image_path: str) -> str:
    """
    Main processing function which uses another basic functions
    :param image_path: image path with license plate
    :return: obtained license plate
    """
    image = cv2.imread(str(image_path))
    if image is None:
        raise LicensePlateException(f'Error loading image from {image_path}')

    plate_outlines, resized_image = preprocessing(image)
    trans_plate = plate_transform(plate_outlines, resized_image)
    preletters = plate_processing(trans_plate)
    letters = letters_processing(preletters, trans_plate)
    final_result = final_processing(letters)
    return final_result


def preprocessing(image: np.ndarray) -> tuple:
    """
    Function resizes the original image, converts it to grayscale
    and looks for outlines to find the license plate in the image
    :param image: original image
    :return resized: resized image with outlined license plate
    :return plates: copies of resized images to other operations
    """
    dim = (640, 480)
    resized = cv2.resize(image, dim, interpolation=cv2.INTER_AREA)
    if SHOW_STEPS:
        cv2.imshow("1 - Original image", resized)

    # This copy of image will be useful in another function
    plates = resized.copy()

    # We have to convert the image to grayscale because we need to use it in the bilateral filter
    gray = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)
    b_filter = cv2.bilateralFilter(gray, 9, 17, 17)
    if SHOW_STEPS:
        cv2.imshow("2 - BilateralFilter", b_filter)

    # To detect outlines, we use Canny detector
    edged = cv2.Canny(b_filter, 30, 250)
    if SHOW_STEPS:
        cv2.imshow("3 - EdgeDetection", edged)

    cnts, new = cv2.findContours(edged.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

    # We sort found contours and then surround those with specified dimensions with rectangle and draw them on the image
    cnts = sorted(cnts, key=cv2.contourArea, reverse=True)[:30]
    for c in cnts:
        # Approximate the contour
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.008 * peri, True)
        x, y, w, h = cv2.boundingRect(c)
        if w > 200:
            if len(approx) < 50:  # Select the contour with less than 50 corners
                NumberPlateCnt = approx  # This is our approx Number Plate Contour
                # Drawing detected contours with a green colour
                cv2.drawContours(resized, [NumberPlateCnt], -1, (0, 255, 0), 3)
            break
    if SHOW_STEPS:
        cv2.imshow("4 - DrawPlate", resized)
    return resized, plates


def plate_transform(resized, plates) -> tuple:
    """
    Making a perspective transformation for the detected license plate
    :param resized: resized image with outlined license plate
    :param plates: copies of resized images to perspective transformation
    :return dst: cut license plate from original image
    """
    resized_copy = resized.copy()
    resized_col = np.shape(resized)[0]
    resized_row = np.shape(resized)[1]

    # Leaving only this part of the photo that has been framed in green
    # we had marked the detected license plates with green
    for i in range(0, int(resized_col) - 1):
        for j in range(0, int(resized_row) - 1):
            if resized[i, j, 0] == 0 and resized[i, j, 1] == 255 and resized[i, j, 2] == 0:
                resized_copy[i, j, :] = 255
            else:
                resized_copy[i, j, :] = 0
    # We define the boundaries of the frames once again in order to define their vertex points
    resized_copy_gray = cv2.cvtColor(resized_copy, cv2.COLOR_BGR2GRAY)
    conts, news = cv2.findContours(resized_copy_gray, 1, 2)
    conts = sorted(conts, key=cv2.contourArea, reverse=True)[:20]
    x_0, y_0, w_0, h_0 = cv2.boundingRect(conts[0])
    area_0 = w_0 * h_0
    rectangled = cv2.minAreaRect(conts[0])
    boxi = cv2.boxPoints(rectangled)
    boxi = np.int0(boxi)
    points = boxi

    # We check whether the detected contour does not apply to the entire image and
    # perform a perspective transformation of the designated plate in order to straighten it
    for c in conts:
        x, y, w, h = cv2.boundingRect(c)
        rectangled = cv2.minAreaRect(c)
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.008 * peri, True)
        if len(conts) > 1:
            if 3 < len(approx) < 10:
                if w > 150 and h > 5 and w / h >= 1.5:
                    boxi = cv2.boxPoints(rectangled)
                    area = w * h
                    boxi = np.int0(boxi)
                    if area_0 >= area > 200:
                        points = boxi
    cv2.drawContours(resized, [points], 0, (0, 0, 255), 2)
    # The new vertex coordinates where the plate is pasted
    v1 = [0, 0]
    v2 = [600, 0]
    v3 = [0, 200]
    v4 = [600, 200]
    vertexes = np.float32([v1, v2, v4, v3])
    rect = np.zeros((4, 2), dtype="float32")
    s = points.sum(axis=1)
    rect[0] = points[np.argmin(s)]
    rect[2] = points[np.argmax(s)]
    diff = np.diff(points, axis=1)
    rect[1] = points[np.argmin(diff)]
    rect[3] = points[np.argmax(diff)]

    # Making a final perspective transformation for license plate and saving it to the "dst" image
    M = cv2.getPerspectiveTransform(rect, vertexes)
    dst = cv2.warpPerspective(plates, M, (600, 220))
    if SHOW_STEPS:
        cv2.imshow("5 - LicensePlate", dst)
    return dst


def plate_processing(dst) -> list:
    """
    Looking for letters and saving contours of those. Additionally, removing blue EU mark from license plate
    :param dst: cut license plate ready to processing
    :return boxes: returning list of boxes with detected letters, sorted by coordinates of their vertexes
    """
    # Conversion from BGR to HSV color space to remove the blue stripe with country mark
    hsv = cv2.cvtColor(dst, cv2.COLOR_BGR2HSV)
    lower_blue = np.array([100, 50, 50])
    upper_blue = np.array([180, 255, 255])
    mask = cv2.inRange(hsv, lower_blue, upper_blue)

    # Finding contours of the letters on the license plate and using hierarchy to remove inside outlines
    gray_dst = cv2.cvtColor(dst, cv2.COLOR_BGR2GRAY)
    numbers = cv2.bilateralFilter(gray_dst, 11, 17, 17)
    adapt = cv2.adaptiveThreshold(numbers, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 15, 2)
    if SHOW_STEPS:
        cv2.imshow("6 - Thresholding", adapt)

    adapt = cv2.add(mask, adapt)
    cts, hierarchy = cv2.findContours(adapt, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE, hierarchy=True)
    conts_list = []
    for i, h in enumerate(hierarchy[0]):
        if h[3] == -1:
            conts_list.append(cts[i])
    boxes = []
    boxes.clear()
    for c in conts_list:
        x, y, w, h = cv2.boundingRect(c)
        ratio = h / w
        rect = cv2.minAreaRect(c)
        box = cv2.boxPoints(rect)
        box = np.int0(box)
        if 40 < h < 200 and 10 < w < 200 and 1.2 < ratio < 5.5 and w * h > 3500:
            boxes.append(box)
    boxes = sorted(boxes, key=lambda boxes: boxes[0][0])
    return boxes


def letters_processing(boxes, dst) -> list:
    """
    Making perspective transformation of the letters to straighten them and extract letters
    :param boxes: list of boxes with detected letters
    :param dst: cut license plate
    :return adapt_letter: list containing the characters prepared for a final match
    """
    letters_list = []
    letters_list.clear()
    for box in boxes:
        v1 = [0, 0]
        v2 = [CHAR_WIDTH, 0]
        v3 = [0, CHAR_HEIGHT]
        v4 = [CHAR_WIDTH, CHAR_HEIGHT]
        vertexes = np.float32([v1, v2, v4, v3])
        rect = np.zeros((4, 2), dtype="float32")
        s = box.sum(axis=1)
        rect[0] = box[np.argmin(s)]
        rect[2] = box[np.argmax(s)]
        diff = np.diff(box, axis=1)
        rect[1] = box[np.argmin(diff)]
        rect[3] = box[np.argmax(diff)]
        M = cv2.getPerspectiveTransform(rect, vertexes)
        letter = cv2.warpPerspective(dst, M, (CHAR_WIDTH, CHAR_HEIGHT))
        # Appending transformed letters to the list
        letters_list.append(letter)

    # Checking how many contours were detected and rejecting those that are disturbances
    if len(boxes) < 7:
        for i in range(0, 7 - len(boxes)):
            letters_list.append(np.zeros((CHAR_HEIGHT, CHAR_WIDTH), dtype="float32"))
    if len(boxes) > 7:
        for i in range(0, len(boxes) - 7):
            del letters_list[-1]

    # Processing letters with morphological operations to remove noises and make negatives easier to detect
    adapt_letter = []
    adapt_letter.clear()
    for letter in letters_list:
        if letter.all() != 0:
            gray_letter = cv2.cvtColor(letter, cv2.COLOR_BGR2GRAY)
            number = cv2.GaussianBlur(gray_letter, (5, 5), 0)
            number = cv2.adaptiveThreshold(number, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 19, 9)
            kernel = np.ones((3, 3), np.uint8)
            #number = cv2.morphologyEx(number, cv2.MORPH_CLOSE, kernel)
            number = cv2.dilate(number, kernel, iterations=1)
            number = cv2.erode(number, kernel, iterations=1)
            number = number / 255
            adapt_letter.append(number)
        else:
            adapt_letter.append(np.zeros((CHAR_HEIGHT, CHAR_WIDTH), dtype="uint8"))
    return adapt_letter


def final_processing(adapt_letter) -> str:
    """
    Classification of signs on license plate.
    Additionally, implementation of rules for creating Polish license plates.
    :param adapt_letter: list of graphically processed characters
    :return final_list: string of licence plate
    """
    final_list = ""
    first_part_len = 2
    # forbidden letters in first part of license plate and theirs corresponding matching
    forbidden_chars_1 = {'0': 'O', '1': 'I', '2': 'Z', '3': 'B', '4': 'A', '5': 'S',
                         '6': 'G', '7': 'Z', '8': 'B', '9': 'P', 'X': 'K'}

    # forbidden letters in second part of license plate and theirs corresponding matching
    forbidden_chars_2 = {'B': '8', 'D': '0', 'I': '1', 'O': '0', 'Z': '2'}

    # Checking the best match for the cut letters and those from the reference list
    for letter in adapt_letter:
        #plt.imshow(np.array(letter).reshape(CHAR_HEIGHT, CHAR_WIDTH))
        #plt.show()

        # Comparing letters on the licence plate to recognition by neural network
        final_list += CHARSET[nn.estimate(letter)]

    print(f"Plate before rules checking = {final_list}")

    # if any of first two characters is number change it to corresponding letters
    for i in range(first_part_len):
        if final_list[i] in forbidden_chars_1:
            new_char = forbidden_chars_1[final_list[i]]
            s = list(final_list)
            s[i] = new_char
            final_list = "".join(s)

    # check second part of license plate
    for i in range(first_part_len, len(final_list)):
        if final_list[i] in forbidden_chars_2:
            new_char = forbidden_chars_2[final_list[i]]
            s = list(final_list)
            s[i] = new_char
            final_list = "".join(s)

    cv2.waitKey(0)
    cv2.destroyAllWindows()
    print(f"Plate = {final_list}")
    return final_list


class LicensePlateException(Exception):
    def __init__(self, message):
        super(LicensePlateException, self).__init__(message)